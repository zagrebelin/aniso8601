import unittest

from aniso8601 import parse_datetime
from aniso8601.exceptions import ISOFormatError


class TestDatetimeResolutionFunctions(unittest.TestCase):
    def test_parse_wrong_datetime(self):
        tests = ('', None, 'wrong', 1)
        for test in tests:
            with self.assertRaises(ISOFormatError):
                result = parse_datetime(test)
                self.assertIsNone(result)
